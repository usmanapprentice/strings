function CheckBracket (str) {
	str = str[0].split('');
	let OpenBracketCount = 0, ClosedBracketCount = 0;
	for(i = 0; i < str.length  ; i += 1){
		if (str[i]=='(') {
			OpenBracketCount += 1;
		}else if (str[i]==')') {
			ClosedBracketCount += 1;
		}
	}
	return ((OpenBracketCount == ClosedBracketCount)?'Correct':'Incorrect');
}
console.log(CheckBracket([ ')(a+b))' ]));