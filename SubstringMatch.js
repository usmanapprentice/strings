function CountMatches (argument) {
	let key = argument[0], string = argument[1];
	let lastFound = -1, matches = 0, index = 0;
	while (true) {
		index = string.indexOf(key, lastFound + 1);
		if (index > 0) {
			matches += 1;
			lastFound = index;
		} else {
			break;
		}
	}
	console.log(matches);
}
CountMatches([
	'in',
	'We are living in an yellow submarine. We don\'t have anything else. inside the submarine is very tight . So we are drinking all the day. We will move out of it in 5 days.'
]);